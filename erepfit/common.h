#ifndef COMMON_H
#define COMMON_H


#include <QString>
#include "basic_types.h"

namespace Erepfit{namespace Input{

    class Coordinate
    {
    public:
        QString symbol;
        dVector3 coord;
    };

}}

#endif // COMMON_H
