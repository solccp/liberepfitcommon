#ifndef EVALUATOR_INPUT_H
#define EVALUATOR_INPUT_H

#include <QDir>
#include <QFileInfo>
#include <QMap>
#include <QString>
#include <QTextStream>
#include <variantqt.h>

#include <QDataStream>

#include "basic_types.h"

namespace Erepfit{namespace Input{

    class ToolChain
    {
    public:
        QString name = "dftb+";
        QString path = "dftb+";
        QVariantMap options;
    };


    class Option
    {
    public:
        ToolChain toolchain;
        bool use_cache = false;
    };


    class Geometry
    {
    public:
        bool loadGen(const QString& filename);
        bool loadGen(QTextStream& fin);
        bool loadXYZ(const QString& filename);
        QStringList getElements() const;
        QStringList getAtoms() const;
        QString getGen() const;
        QString getXYZ() const;
        QString getDCDFTBKXYZ() const;
        QString getOldVersionXYZ() const;
        dVector3 getCoordinate(int index) const;
        void reset();

    public:
        //Crystal
        dMatrix3x3 lattice_vectors;
        double scaling_factor = 1.0;

        ADPT::KPointsSetting kpoints;

        bool fractional_coordinates = true;

        //Molecule
        int charge = 0;
        int spin = 1;

        //Common
        QList<Coordinate> coordinates;

        bool isPBC = false;
        bool spin_polarization = false;
        double unpaired_electrons = 0.0;
    };

    class SystemElectronicData
    {
    public:
        QList<dVector3> force;
        bool pbc = false;
        dMatrix3x3 stress;
        double energy = 0.0;
        double efermi_a = 0.0;
        double efermi_b = 0.0;
    };

    class System
    {
    public:
        Geometry geometry;
        QString template_input;
        QString name;
        ADPT::UUID uuid;
        bool evaluated = false;
        SystemElectronicData elec_data;
        bool opt_upe = false;
    };

    class AtomicEnergy
    {
    public:
        QMap<QString, QString> atomic_dftbinputs;
        QMap<QString, double> atomic_energy;
        bool isAtomDefined(const QString& symbol) const
        {
            if (atomic_energy.contains(symbol) || atomic_dftbinputs.contains(symbol))
            {
                return true;
            }
            return false;
        }
    };

    class Equation
    {
    public:
        QList<EnergyEquation> energyEquations;
        QList<ForceEquation> forceEquations;
        QList<ReactionEquation> reactionEquations;
        QMap<QString, QList<AdditionalEquation>> additionalEquations;
    };

    class Erepfit2_InputData
    {
    public:
        Erepfit2_InputData() = default;
        Erepfit2_InputData(const Erepfit2_InputData& rhs);

        AtomicEnergy atomicEnergy;
        QMap<QString, PotentialGrid> potential_grids;
        Equation equations;
        QList<System> systems;
        Option options;
        QVariantMap dftb_options;

        std::shared_ptr<ADPT::SKFileInfo> electronic_skfiles;
        QMap<QString, QString> external_repulsive_potentials;

    public:
        QSet<QString> required_elec_pairs() const;
        QStringList required_elements() const;
        void makeAbsolutePath(const QDir& rootDir);
        void symmetrize_external_repulsive_potentials();
    };

}}



#endif // EVALUATOR_INPUT_H
