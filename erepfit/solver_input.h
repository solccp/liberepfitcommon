#ifndef SOLVER_INPUT_H
#define SOLVER_INPUT_H

#include "basic_types.h"

//#include <QDir>
//#include <QFileInfo>
//#include <QMap>
//#include <QString>
//#include <QTextStream>
//#include <variantqt.h>

//#include <QDataStream>



#include <QList>

namespace Erepfit{namespace Input{


    class PotentialGrid
    {
    public:
        QList<double> knots;
    };


    class EnergyEquation
    {
    public:
        QString name;
        ADPT::UUID uuid;
        double energy;
        double weight = 1.0;
        QString unit = "kcal/mol";
    };

    class ForceEquation
    {
    public:
        QString name;
        ADPT::UUID uuid;
        double weight = 1.0;
        bool has_reference_force = false;
        QList<dVector3> reference_force;
    };


    class StressEquation
    {
    public:
        QString name;
        ADPT::UUID uuid;
        double weight = 1.0;
        bool has_reference_stress = false;
        QList<dMatrix3x3> reference_stress;
    };


    class AdditionalEquation
    {
    public:
        QString potential;
        double distance;
        int derivative;
        double value;
        double weight = 1.0;
        QString unit = "bohr";
    };

    class ReactionItem
    {
    public:
        QString name;
        ADPT::UUID uuid;
        double coefficient;
    };

    class ReactionEquation
    {
    public:
        QList<ReactionItem> reactants;
        QList<ReactionItem> products;
        double weight = 1.0;
        double energy;
        QString name;
        ADPT::UUID uuid;
        QString unit = "kcal/mol";
    };


    class Option
    {
    public:
        bool debug = false;
        int spline_order = 4;
        int continous_order = 3;
    };



}}



#endif // SOLVER_INPUT_H
